/*
** Class:  TestSendCaseEmail
** Created by OpFocus on Jan 20, 2016
** Description: Unit tests for the TestSendCaseEmail page and controller
**              
*/  
@isTest
private class TestSendCaseEmail {

    private static Boolean TestSendCaseEmail         = true;

    static testMethod void TestSendCaseEmail() {

    	System.assert(TestSendCaseEmail, 'Test disabled.');

    	EmailTemplate testEmailTemplate = new EmailTemplate(IsActive = true, Name = 'name', DeveloperName = 'test_unique_name', TemplateType = 'text', FolderId = UserInfo.getUserId(), Subject = 'Your Subject Here');
		insert testEmailTemplate;

    	Account a = new Account(Name='Test Account');
    	insert a;

    	Contact primaryContact = new Contact(AccountId = a.Id, LastName = 'McTest', Email='Test1@test.com');
    	Contact contact2 = new Contact(AccountId = a.Id, LastName = 'LaTest', Email='Test2@test.com');
    	Contact contact3 = new Contact(AccountId = a.Id, LastName = 'D\'Test', Email='Test3@test.com');
    	Contact contact4 = new Contact(AccountId = a.Id, LastName = 'Test', Email='Test4@test.com');
    	Contact contact5 = new Contact(AccountId = a.Id, LastName = 'Testerson', Email='Test5@test.com');

    	insert new List<Contact>{primaryContact, contact2, contact3, contact4};

		Case case1 = new Case(AccountId=a.Id, ContactId = primaryContact.Id, Subject='Case 1');
		Case case2 = new Case(AccountId=a.Id, ContactId = primaryContact.Id, Subject='Case 2');
		insert new List<Case>{case1,case2};


    	// Let's load it form an case
		PageReference pgRef = Page.SendCaseEmail;
		pgRef.getParameters().put('CaseId', String.valueOf(case1.Id));
		Test.setCurrentPage(pgRef);
		SendCaseEmailController ctl = new SendCaseEmailController();
		ctl.fromAddressId = UserInfo.getUserId();
		ctl.toContactId = primaryContact.Id;
		ctl.toAddresses = contact2.Email;
		ctl.ccAddresses = contact3.Email;
		ctl.caseStatus = 'Awaiting Fix';
		ctl.emailTemplate = testEmailTemplate.Id;
		ctl.SelectRecipient();
		System.assertEquals('Your Subject Here', ctl.emailSubject);

		ctl.SendEmail();

		//Now let's reload from an email
		EmailMessage newEmail = new EmailMessage(BccAddress=contact4.Email, CcAddress=contact3.Email, ParentId=case1.Id, FromAddress=UserInfo.getUserEmail(), FromName=UserInfo.getUserName(), HtmlBody='Test Email body', Subject='Test Subject', ToAddress=primaryContact.Email);
		insert newEmail;

		pgRef = Page.SendCaseEmail;
		pgRef.getParameters().put('EmailId', String.valueOf(newEmail.Id));
		pgRef.getParameters().put('reply', String.valueOf('all'));
		
		Test.setCurrentPage(pgRef);
		ctl = new SendCaseEmailController();
		ctl.toContactId = primaryContact.Id;
		ctl.toAddresses = contact2.Email;
		ctl.ccAddresses = contact3.Email;
		ctl.bccAddresses = contact4.Email;
		ctl.caseStatus = 'Awaiting Fix';

		// Test that the page can poll for case updates after page was loaded
		CaseComment cComment = new CaseComment(ParentId=case1.Id, CommentBody='test');
		insert cComment;

		EmailMessage secondEmail = newEmail.clone();
		insert secondEmail;

		//check for updates;
		ctl.pollForCaseUpdates();

		ctl.dummyCase.id = case2.Id;
		ctl.RefreshCase();


		// Cover the events that won't happen normally in a test method
		ctl.cancel();
		ctl.AttachFile();
    	
    }
}