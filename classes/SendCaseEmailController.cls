/*
** Class:  SendCaseEmailController
** Created by OpFocus on Dec 23, 2015
** Description: Contorller for the custom SendCaseEmail page
**   
*/  
public without sharing class SendCaseEmailController {

	// If there is a specific folder we want to list templates from specifiy it here.
	// Blank will give 'Unfiled Public Email Templates'
	private static final String templateFolder = '';

	public Boolean redirectToCaseAfterSend = true;

	public Boolean pageError{get; private set;}
	public List<SelectOption> fromAddressesOptions{get; private set;}
	public Id fromAddressId {get; set;}

	public List<SelectOption> toContactIdOptions{get; private set;}	
	public Id toContactId {get; set;}

	public List<SelectOption> toAddressesOptions{get; private set;}	
	public String toAddresses {get; set;}

	public List<SelectOption> ccAddressesOptions{get; private set;}	
	public String ccAddresses {get; set;}

	public List<SelectOption> bccAddressesOptions{get; private set;}	
	public String bccAddresses {get; set;}

	public List<SelectOption> caseStatusOptions{get; private set;}	
	public String caseStatus {get; set;}

	public List<SelectOption> emailTemplateOptions{get; private set;}	
	public String emailTemplate {get; set;}

	public List<SelectOption> caseOwnerOptions{get; private set;}	

	public Boolean showResults{get; private set;}
	public String sendResults{get; private set;}

	public Datetime timePageWasLoaded {get; private set;}

	// Used for rendering look-up dialogs
	public Case dummyCase {get; set;}
	public String emailSubject {get; set;}
	public String emailBody {get; set;}
	public Boolean emailIsPlainText{get;set;}
	public Id caseId {get; private set;}
	public Case thisCase {get; private set;}
	public EmailMessage thisEmail{get; private set;}
	public Id emailId {get; private set;}
	public String returnID {get; private set;}
	public Boolean replyAll {get; private set;}

	public Map<Id, String> mapToAddress {get; private set;}
	public Map<Id, String> mapFromAddress {get; private set;}

	public SendCaseEmailController() {
		timePageWasLoaded = Datetime.now();
		init();
		if(ApexPages.currentPage().getParameters().containsKey('CaseId')){
			caseId = ApexPages.currentPage().getParameters().get('CaseId');
		}else if(ApexPages.currentPage().getParameters().containsKey('EmailId')){
			emailId = ApexPages.currentPage().getParameters().get('EmailId');
			if(ApexPages.currentPage().getParameters().containsKey('reply')){
				if(ApexPages.currentPage().getParameters().get('reply') == 'all'){
					replyAll = true;
				}
			}
		}
		if(ApexPages.currentPage().getParameters().containsKey('retURL')){
			returnID = ApexPages.currentPage().getParameters().get('retURL');
		}
		emailIsPlainText = false;
        caseStatusOptions = getCaseStatuses();
		if(String.isNotBlank(caseId)){
			loadCase();
		}else if(String.isNotBlank(emailId)){
			loadFromEmail();
		}
	}

	public void init(){
		emailTemplateOptions = getEmailTemplates();
		emailTemplate = 'Awaiting Customer Response';
		dummyCase = new Case();
		dummyCase.OwnerId = UserInfo.getUserId();
		pageError = false;
	}

	// Any field valiation that you want to do before doing the send
	public Boolean validateFields(){
		/* currently there is no fields we need to validate.  But if we do you check them and then add the Page Message here
		if (someCondtionToCheck = true) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Here is your error message');
			ApexPages.addMessage(msg);
			return false;
		}
		*/
		return true;
	}

	public PageReference SendEmail(){

		if(validateFields() == false){
			// Error messages will be set in the validatFields method
			return null;			
		}

		// Send the actual email
		Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
		List<String> lstToAddresses = new List<String>();
		List<String> lstCcAddresses = new List<String>();
		List<String> lstBccAddresses = new List<String>();

		lstToAddresses.add(mapToAddress.get(dummyCase.ContactId));
		for(String to : toAddresses.split(',\\s') ) {
			String addy = to.replace('[', '').replace(']', '');
			if(String.isNotBlank(addy)){
				lstToAddresses.add( addy );
			}
		}

		emailToSend.setToAddresses(lstToAddresses);
		
		for(String cc : ccAddresses.split(',\\s') ) {
			String addy = cc.replace('[', '').replace(']', '');
			if(String.isNotBlank(addy)){
				lstCcAddresses.add( addy );
			}
		}
		if(lstCcAddresses.isEmpty() == false){
			emailToSend.setCcAddresses(lstCcAddresses);
		}

		for(String bcc : bccAddresses.split(',\\s')){
			String addy = bcc.replace('[', '').replace(']', '');
			if(String.isNotBlank(addy)){
				lstBccAddresses.add( addy );
			}
		}
		if(lstBccAddresses.isEmpty() == false){
			emailToSend.setBccAddresses(lstBccAddresses);
		}

		emailToSend.setSubject(emailSubject);
		if(emailIsPlainText){
			emailToSend.setPlainTextBody(emailBody);
		}else{
			emailToSend.setHTMLBody(emailBody);
		}
		if(fromAddressId == UserInfo.getUserId()){
			emailToSend.setToAddresses(new List<String> {mapFromAddress.get(fromAddressId)});
		}else{
			emailToSend.setOrgWideEmailAddressId(fromAddressId);
		}

		List<Messaging.SendEmailResult> sentEmailResults;
		try{
			sentEmailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{emailToSend});
		// Error handling of Messaging.sendEmail function
		}catch(System.EmailException ee){
			System.debug('============> EmailException: ' + ee);
			String err = 'The email ' + mapFromAddress.get(fromAddressId) + ' address has not be verified for use in Salesforce, please select another From Address.  Configuration of the from email addresses can be found under the \'Organization-Wide Email Addresses\' settings';
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			return null;
		}catch(Exception e){
			System.debug('============> Exception: ' + e);
			String err = 'There was a problem sending the email  (' + thisCase.Subject + ')';
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			return null;
		}
		
		// Error handling on a per-message level
		for(Messaging.SendEmailResult sentEmailResult: sentEmailResults){
			if(sentEmailResult.isSuccess() == false){
				pageError = true;
				System.debug('============> sentEmailResult: ' + sentEmailResult);
				String err = 'There was a problem sending the email (' + thisCase.Subject + ')';
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
				ApexPages.addMessage(msg);
			}
		}

		// Create the Email Message record
		EmailMessage emailRecord = new EmailMessage();
		emailRecord.FromAddress = mapFromAddress.get(fromAddressId);
		emailRecord.FromName = mapFromAddress.get(fromAddressId);
		emailRecord.ToAddress = String.join(lstToAddresses, ';');
		emailRecord.CcAddress = String.join(lstCcAddresses, ';');
		emailRecord.BccAddress = String.join(lstBccAddresses, ';');
		emailRecord.Subject = emailSubject;
		if(emailIsPlainText){
			emailRecord.TextBody = emailBody;
		}else{
			emailRecord.HtmlBody = emailBody;
		}
		emailRecord.ParentId = caseId; 
		insert emailRecord;

		if(pageError == false){
			sendResults = 'Email sent sucessfully';
		}else{
			sendResults = 'There was a problem sending the email';
		}
		
		if(redirectToCaseAfterSend == true){
			showResults = false;
			return new ApexPages.standardController(thisCase).view();
		}else{
			showResults = true;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, sendResults);
			return null;
		}

	}


	public PageReference SelectRecipient(){
		if(String.isNotBlank(emailTemplate)){
			SelectTemplate();
		}
		return null;
	}

	public PageReference SelectTemplate(){
		if(String.isNotEmpty(emailTemplate)){
			Messaging.SingleEmailMessage dummyMessage = createMergedEmail(dummyCase.ContactId, dummyCase.ParentId, fromAddressId, emailTemplate);
			if(String.isBlank(dummyMessage.getPlainTextBody()) == false){
				emailBody = dummyMessage.getPlainTextBody();
				emailIsPlainText = true;
			}
			if(String.isBlank(dummyMessage.getHTMLBody()) == false){
				emailBody = dummyMessage.getHTMLBody();
				emailIsPlainText = false;
			}
			emailSubject = dummyMessage.getSubject();
		}
		return null;
	}

	public PageReference RefreshCase(){
		toAddresses = null;
		ccAddresses = null;
		bccAddresses = null;
		loadCase();
		return null;
	}

	// TODO: Not implimented yet
	public PageReference attachFile(){
		
		return null;
	}

	public PageReference cancel(){
		if(returnID == null){
			return null;
		}
		return new PageReference(returnID);
	}


	private List<SelectOption> getFromAddressOptions(){
		mapFromAddress = new Map<Id, String>();
		List<SelectOption> lstOptions = new List<SelectOption>();
		// Add current user
		lstOptions.add(new SelectOption(UserInfo.getUserId(), UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ' (' + UserInfo.getUserEmail() + ')'));
		mapFromAddress.put(UserInfo.getUserId(), UserInfo.getUserEmail());
		//Add Org Wide Emails
		for(OrgWideEmailAddress address : [select id, Address from OrgWideEmailAddress]){
			lstOptions.add(new SelectOption(address.Id, address.Address));
			mapFromAddress.put(address.Id, address.Address);
		}
		return lstOptions;
	}

	private List<SelectOption> getCaseOwnerOptions(){
		List<SelectOption> lstOptions = new List<SelectOption>();
		// First get internal Users
		for(User user : [select Id, Name from User where IsActive = true ]){
			lstOptions.add(new SelectOption(user.Id, user.Name));
		}
		// Next get Queues
		for(Group queue : [select Id, Name from Group where Type='Queue']){
			lstOptions.add(new SelectOption(queue.Id, queue.Name));
		}
		return lstOptions;
	}

	// We will be able to send to any Contact assocatited with the same Account
	private List<SelectOption> getSendToOptions(Boolean useContactId){
		mapToAddress = new Map<Id, String>();
		List<SelectOption> lstOptions = new List<SelectOption>();
		List<Contact> lstContacts = [Select Name, Id, Email, AccountId from Contact where (AccountId = :thisCase.AccountId) and Email != null];
		for(Contact c: lstContacts){
			String value;
			if(useContactId){
				value = c.Id;
			}else{
				value = c.Email;
			}
			lstOptions.add(new SelectOption(value, c.Name + ' (' + c.Email + ')' ));
			mapToAddress.put(c.Id, c.Email);
		}
		return lstOptions;
	}

	// We can update the status of the case
	private static List<SelectOption> getCaseStatuses(){
		List<SelectOption> lstOptions = new List<SelectOption>();
		Map<String, Schema.SObjectField> field_map = Case.getSObjectType().getDescribe().fields.getMap(); //get a map of fields for the passed sobject
		List<Schema.PicklistEntry> pick_list_values = field_map.get('Status').getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
			lstOptions.add(new selectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
		}
		return lstOptions;
	}

	//
	private static List<SelectOption> getEmailTemplates(){
		List<SelectOption> lstOptions = new List<SelectOption>();
		lstOptions.add(new selectOption('', ''));
		Map<String, Schema.SObjectField> field_map = Case.getSObjectType().getDescribe().fields.getMap(); //get a map of fields for the passed sobject
		List<EmailTemplate> lstTemplates = [SELECT Name, Id, Body, IsActive, Folder.Name FROM EmailTemplate where Folder.Name = :templateFolder];
		for (EmailTemplate t : lstTemplates) { 
			lstOptions.add(new selectOption(t.Id, t.Name)); //add the value and label to our final list
		}
		return lstOptions;
	}


	// Load the info of the current case. Used on inital load and when the user selects a new case
	private void loadCase(){
		thisCase = [select Id, ParentId, ContactId, AccountId, Status, Subject from Case where Id = :caseId];
		caseOwnerOptions = getCaseOwnerOptions();
		fromAddressesOptions = getFromAddressOptions();
		toContactIdOptions = getSendToOptions(true);
		toAddressesOptions = getSendToOptions(false);
		ccAddressesOptions = toAddressesOptions;
		bccAddressesOptions = toAddressesOptions;
		dummyCase.parentId = thisCase.Id;
		dummyCase.contactId = thisCase.ContactId;
		caseStatus = thisCase.Status;
	}

	// Load the info of the current case. Used on inital load and when the user selects a new case
	private void loadFromEmail(){
		thisEmail = [SELECT ActivityId, BccAddress, CcAddress, ParentId, CreatedById, CreatedDate, IsDeleted, Id, FromAddress, FromName, HtmlBody, HasAttachment, Headers, Incoming, LastModifiedById, LastModifiedDate, MessageDate, Status, Subject, SystemModstamp, TextBody, ToAddress FROM EmailMessage where Id = :emailId];
		caseId = thisEmail.ParentId;
		loadCase();

		if(String.isBlank(thisEmail.TextBody) == false){
			emailBody = thisEmail.TextBody;
			emailIsPlainText = false;
		}
		if(String.isBlank(thisEmail.HtmlBody) == false){
			emailBody = thisEmail.HTMLBody;
			emailIsPlainText = true;
		}
		emailSubject = thisEmail.Subject;
		toAddresses = thisEmail.ToAddress;
		if(replyAll == true){
			toAddresses = this.toAddresses;
			ccAddresses = thisEmail.CcAddress;
			bccAddresses = thisEmail.BccAddress;
		}
	}

	// If we are using a template then this is how we do the mail-merge to get the body of the email
	private Messaging.SingleEmailMessage createMergedEmail(Id contactIdToSendTo, Id caseIdToSend, Id fromEmailAddressId, Id templateId){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		Messaging.SingleEmailMessage mergedEmail = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(contactIdToSendTo);
		email.setWhatId(caseIdToSend);

		email.setTemplateId(templateId);

		// Do a "fake" send so that we do the merge.  
		List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage> ();
		lstEmails.add(email);
		Savepoint sp = Database.setSavepoint();
		List<Messaging.SendEmailResult> stagingEmailResults = Messaging.sendEmail(lstEmails, false);
		Database.rollback(sp);	

		// Now that we have the merge done, copy it over the the new email that we actually work with.
		List<String> toEmailsAddresses = new List<String>();
		if(email.getToAddresses() != null){
			toEmailsAddresses.addAll(email.getToAddresses());
		}

		if(String.isNotBlank(email.getPlainTextBody())){
			emailIsPlainText = true;
			mergedEmail.setPlainTextBody(email.getPlainTextBody());
		}
		if(String.isNotBlank(email.getHTMLBody())){
			emailIsPlainText = false;
			mergedEmail.setHTMLBody(email.getHTMLBody());
		}
		
		mergedEmail.setSubject(email.getSubject());	

		return mergedEmail;
	}

	// Periodically we want to check and see if there has been any updates or case activity since we've loaded the page
	public PageReference pollForCaseUpdates(){
		//See if the case has been updated since we loaded
		Case refreshedCase = [Select Id, LastModifiedDate, LastModifiedById, LastModifiedBy.Name from Case where Id = :thisCase.Id];	

		if(refreshedCase.LastModifiedDate > timePageWasLoaded){
			String messageText = 'The case has been updated ' + refreshedCase.LastModifiedBy.Name + ' at ' + refreshedCase.LastModifiedDate.format('HH:mm a');
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, messageText);
			ApexPages.addMessage(msg);			
		}

		//Now see if there's any new Case Comments
		List<CaseComment> lstCaseComments = [SELECT CommentBody, ParentId, Id, CreatedDate, CreatedById, CreatedBy.Name FROM CaseComment where ParentId = :thisCase.Id and CreatedDate > :timePageWasLoaded order by CreatedDate desc];
		for(CaseComment cc : lstCaseComments){
			String messageText = 'A new comment had been addded to this case at ' + cc.CreatedDate.format('HH:mm a') + ' by ' + cc.CreatedBy.Name  + '.';
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, messageText);
			ApexPages.addMessage(msg);					
		}
		return null;
	}

}